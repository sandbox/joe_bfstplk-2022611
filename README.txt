Provides Smarter Blox, which can be placed on pages or in Panels, Mini-Panels,
etc., via an include/exclude pattern based on the page's Drupal-relative URL
matching a URL pattern in the include list and not matching a URL pattern in
the exclude list.

The basic idea of this module is to use the page path itself in a manner
similar to a taxonomy term, with the standard Drupal URL list parser allowing
use of site-relative URLs and wildcard patterns per Drupal's standard.  It
allows you to place a Smarter Blox block in your page template or anywhere
blocks can be used, and to have the block content "know" whether it is supposed
to appear, based on the URL of the page displaying it.  One Smarter Blox block
can take the place of a legion of regular Drupal blocks, which prevents the
Block Administration page from being bogged down with loading all those blocks
and their ordering drag-handles and configuration links separately (which is
the issue that originally spawned the idea for this module).

* Upon installation, this module creates its own content type ("smarter_blox"),
which has the fields required for the Smarter Blox content to "know" where it
is and isn't supposed to show up, based on a URL inclusion/exclusion pattern
and the selected "Show in Blox" index.

* The administration page for this module allows the admin to preset the number
of Smarter Blox blocks available in the Blocks Administration panel and
elsewhere where Blocks can be inserted (Panels, Mini-Panels, etc.).

* Smarter Blox blocks are named "Smarter Blox X", where "X" is the numerical
index of the Smarter Blox in question.  This aids the user in selecting the
appropriate value for the "Show in Blox" field when creating or editing a piece
of Smarter Blox content.  The list is zero-indexed.

* Smarter Blox content items can show up in multiple pages, based on their
inclusion/exclusion pattern, and multiple Smarter Blox content items can appear
in a single Smarter Blox block, if more than one block's inclusion/exclusion
pattern matches the page's URL.

* A "Sort Order" field has been provided to weight items into a particular
order, if the order they were found by Drupal/MySQL is not appropriate for your
needs.

In the attached Smarter Blox Edit page images, you can probably already see how
the inclusion/exclusion pattern works, and how one specifies in which Smarter
Blox block the piece of content is supposed to appear if the page's URL matches
the inclusion pattern and doesn't match any of the exclusion patterns.

For example:  You have a block of HTML indicating the Department Office hours
for the "Department That Goes PING", which you want to appear on
http://yoursite.com/department-goes-ping and all of the pages in that URL
subtree (i.e., http://yoursite.com/department-goes-ping/*) except the
http://yoursite.com/department-goes-ping/faculty page in that subtree.


* Put <code>department-goes-ping department-goes-ping/*</code> in the "Show in
Page" box * Put <code>department-goes-ping/faculty </code> in the "Hide from
Page" box * Select which "Smarter Blox" block you wish the block to appear in *
Adjust sort order if needed


The nice thing about Smarter Blox is that the same Smarter Blox block can also
show the Department Office hours for "The Most Expensive Department in the
Whole College" in that branch of the site and its URL subtree with only one
block in the Block Administration page, which is nice for an almost
"sub-region" effect added to the region where the Smarter Blox block is
deployed in the Block Administration page.  The Panels, Mini-Panels, and Panel
Page interfaces can also use the Smarter Blox block just like a regular block,
so if you have one of those set up to do, for example, the "landing page" at
the front end of your departments, you can use the same template for all of the
departments, and have the content "know" on which department page it is
supposed to appear.
